package aed;

public class SistemaHEAP
{   
    private int[][] heap;
    private int longitudHeap;
    private int punteroALaDerecha;

    public SistemaHEAP(int[][]heapDEsordenado , int longitud)
    {
        //en la posicion 0 se encuentran los votos, en la 1 los ids de partido
        this.heap = heapDEsordenado;      //O(1)
        this.longitudHeap = longitud;     //O(1)
        this.punteroALaDerecha = longitud - 1;  //O(1)

        algoritmoDeFloyd(punteroALaDerecha);

        //Constructor de mi heap
    }

    private void algoritmoDeFloyd(int ultimoElemento) //O(P - 1) en el peor de los casos
    {
        for(int i = ultimoElemento; i >= 0; i--)
        {
            bajarElemento(i); //Log(P - 1) en el peor de los casos
        }

        //Algoritmo de floyd para poder transformar una array desordenada a un heap que cumple su invariante
    }

    private boolean tieneHijoIzquierdo(int punteroPadre)
    {
        boolean hijoIzquierdo = false;  //O(1)

        if((2*punteroPadre) + 1 < longitudHeap)  //O(1)
        {
            hijoIzquierdo = true; //O(1)
        }

        return hijoIzquierdo; //O(1)
    }

    private boolean tieneHijoDerecho(int punteroPadre)
    {
        boolean hijoDerecho = false;  //O(1)

        if((2*punteroPadre) + 2 < longitudHeap)  //O(1)
        {
            hijoDerecho = true;  //O(1)
        }

        return hijoDerecho; //O(1)
    }

    private int obtenerHijoIzquierdo(int punteroPadre)
    {
        int punteroHijoIzquierdo = (2 * punteroPadre) + 1; //O(1)

        return punteroHijoIzquierdo;  //O(1)
    }

    private int obtenerHijoDerecho(int punteroPadre)
    {
        int punteroHijoDerecho = (2*punteroPadre) + 2; //O(1)

        return punteroHijoDerecho; //O(1)
    }

    public int obtenerVotosRaiz()
    {
        return heap[0][0];   //O(1)
    }

    public int obtenerPartidoRaiz()
    {
        return heap[0][1];   //O(1)
    }

    public void introducirNuevoValorRaiz(int nuevoValor)
    {
        heap[0][0] = nuevoValor;     //O(1) 
        bajarElemento(0);    //Log(P - 1) en el peor de los casos

        //Cuando introduzca el nuevo elemento, es necesario ejecutar el algoritmo bajar
    }

    private void bajarElemento(int puntero) //Log(P - 1) en el peor de los casos
    {
        if(tieneHijoDerecho(puntero))    //O(1)
        {
            int punteroHijoDerecho = obtenerHijoDerecho(puntero);    //O(1)
            int[] valorDelPadre = heap[puntero];   //O(1)
            int punteroHijoIzquierdo = obtenerHijoIzquierdo(puntero);  //O(1)

            if(heap[punteroHijoDerecho][0] > heap[puntero][0] && heap[punteroHijoDerecho][0] >= heap[punteroHijoIzquierdo][0])  //O(1)
            {
                heap[puntero] = heap[punteroHijoDerecho]; //O(1)
                heap[punteroHijoDerecho] = valorDelPadre; //O(1)

                bajarElemento(punteroHijoDerecho);
            }
            else if(heap[punteroHijoIzquierdo][0] > heap[puntero][0])  //O(1)
            {
                heap[puntero] = heap[punteroHijoIzquierdo];  //O(1)
                heap[punteroHijoIzquierdo] = valorDelPadre;  //O(1)

                bajarElemento(punteroHijoIzquierdo);
            }
        }
        else if(tieneHijoIzquierdo(puntero))
        {
            int punteroHijoIzquierdo = obtenerHijoIzquierdo(puntero);  //O(1)
            int[] valorPadre = heap[puntero];  //O(1)

            if(heap[punteroHijoIzquierdo][0] > heap[puntero][0])  //O(1)
            {
                heap[puntero] = heap[punteroHijoIzquierdo];  //O(1)
                heap[punteroHijoIzquierdo] = valorPadre; //O(1)

                bajarElemento(punteroHijoIzquierdo);
            }
        }

        //Este algoritmo es el encargado de bajar la raiz del heap, una vez alterada la raiz, la comparo con sus hijos
        //y veo si alguno de ellos es mas grande que el padre
        //Si alguno es mas grande que el padre, intercambio sus posiciones y repito el algoritmo, apuntando a la nueva posicion
        //del padre
        //Si los 2 hijos son mayores que el padre, hago el intercambio con el mayor de sus 2 hijos
        //Si ninguno es mas grande, termina el algoritmo y mi heap cumple el invariante
    }
}
