package aed;
public class SistemaCNE{
    
    private String[] nombresDePartidos;
    private String[] nombresDeDistritos;
    private int[] diputadosDeDistritos;
    private int[] rangoMesas;
    private int[] votosDeLosPresidentes;
    private int[][] votosDeDiputados;
    private int[] cantidadTotalVotosDiputadosPorDistrito;
    private int[][] bancasDiputados;
    private boolean[] resultadosBancasCalculado;
    private SistemaHEAP[] arrayDeHeaps;

    private int primeroEnPresidenciales;
    private int segundoEnPresidenciales;
    private int totalVotosPresidenciales;

    public class VotosPartido{
        private int presidente;
        private int diputados;
        VotosPartido(int presidente, int diputados){this.presidente = presidente; this.diputados = diputados;}
        public int votosPresidente(){return presidente;}
        public int votosDiputados(){return diputados;}
    }

    public SistemaCNE(String[] nombresDistritos, int[] diputadosPorDistrito, String[] nombresPartidos, int[] ultimasMesasDistritos)
    {
        int longitudPartidos = nombresPartidos.length;                          //O(1)
        int longitudDistritos = nombresDistritos.length;                        //O(1)

        nombresDePartidos = new String[longitudPartidos];                       //O(P)
        nombresDeDistritos = new String[longitudDistritos];                     //O(D)
        diputadosDeDistritos = new int[longitudDistritos];                      //O(D)
        rangoMesas = new int[longitudDistritos];                                //O(D)
        votosDeLosPresidentes = new int[longitudPartidos];                      //O(P)
        votosDeDiputados = new int[longitudDistritos][longitudPartidos];        //O(P * D)
        cantidadTotalVotosDiputadosPorDistrito = new int[longitudDistritos];    //O(D)
        bancasDiputados = new int[longitudDistritos][longitudPartidos - 1];     //O(P-1 * D)
        resultadosBancasCalculado = new boolean[longitudDistritos];             //O(D)
        arrayDeHeaps = new SistemaHEAP[longitudDistritos];                      //O(D)

        for(int i = 0; i < longitudPartidos; i++)                               //O(P)
        {
            nombresDePartidos[i] = nombresPartidos[i];                          //O(1)
        }

        for(int i = 0; i < longitudDistritos; i++)                              //O(D)
        {
            nombresDeDistritos[i] = nombresDistritos[i];                        //O(1)
            diputadosDeDistritos[i] = diputadosPorDistrito[i];                  //O(1)
            rangoMesas[i] = ultimasMesasDistritos[i];                           //O(1)
        }

        primeroEnPresidenciales = 0;                                            //O(1)
        segundoEnPresidenciales = 0;                                            //O(1)
        totalVotosPresidenciales = 0;                                           //O(1)

    }

    public String nombrePartido(int idPartido)
    {
        return nombresDePartidos[idPartido]; //O(1)

    }


    public String nombreDistrito(int idDistrito)
    {
        return nombresDeDistritos[idDistrito]; //O(1)

    }

    public int diputadosEnDisputa(int idDistrito) {
        return diputadosDeDistritos[idDistrito]; //O(1)

    }

    public String distritoDeMesa(int idMesa) {
        return nombreDistrito(busquedaBinaria(rangoMesas, idMesa)); //O(Log(D))

    }

    public void registrarMesa(int idMesa, VotosPartido[] actaMesa)
    {
        int distrito = busquedaBinaria(rangoMesas, idMesa); //O(Log(D))

        for(int i = 0; i < actaMesa.length; i++) //O(P)
        {
            int votosDelPresidente = actaMesa[i].votosPresidente(); //O(1)
            int votosDelDiputado = actaMesa[i].votosDiputados();  //O(1)

            votosDeLosPresidentes[i] += votosDelPresidente; //O(1)
            votosDeDiputados[distrito][i] += votosDelDiputado; //O(1)
            cantidadTotalVotosDiputadosPorDistrito[distrito] += votosDelDiputado; //O(1)

            totalVotosPresidenciales += votosDelPresidente; //O(1)
        }

        for(int i = 0; i < votosDeLosPresidentes.length - 1; i++) //O(P-1)
        {
            if(votosDeLosPresidentes[i] > primeroEnPresidenciales)  //O(1)
            {
                primeroEnPresidenciales = votosDeLosPresidentes[i];  //O(1)
            }
        }

        for(int i = 0; i < votosDeLosPresidentes.length - 1; i++) //O(P-1)
        {
            if(votosDeLosPresidentes[i] != primeroEnPresidenciales && votosDeLosPresidentes[i] > segundoEnPresidenciales) //O(1)
            {
                segundoEnPresidenciales = votosDeLosPresidentes[i];  //O(1)
            }
        }
 
        int umbral = (3 * cantidadTotalVotosDiputadosPorDistrito[distrito]) / 100;  //O(1)
        int[][] votosFiltrados = new int[nombresDePartidos.length - 1][2]; //O(2(P - 1)) = O(P)
        int contador = 0;

        for(int i = 0; i < votosDeDiputados[distrito].length - 1; i++)                  //O(P-1)
        {
            if(votosDeDiputados[distrito][i] > umbral)                                //O(1)
            {
                votosFiltrados[contador][0] = votosDeDiputados[distrito][i];        //O(1)
                votosFiltrados[contador][1] = i;                                   //O(1)
                contador += 1;                                                    //O(1)
            }
        }

        SistemaHEAP heapDiputados = new SistemaHEAP(votosFiltrados, contador);  //O(2(P - 1)) = O(P)
        arrayDeHeaps[distrito] = heapDiputados;                                 //O(1)

        if(resultadosBancasCalculado[distrito] == true)                        //O(1)
        {
            for(int i = 0; i < bancasDiputados[distrito].length; i++)          //O(P-1)
            {
                bancasDiputados[distrito][i] = 0;                              //O(1)
            }

            resultadosBancasCalculado[distrito] = false;                       //O(1)
        } 

    }


    public int votosPresidenciales(int idPartido)
    {
        return votosDeLosPresidentes[idPartido]; //O(1)

    }

    public int votosDiputados(int idPartido, int idDistrito)
    {
        return votosDeDiputados[idDistrito][idPartido]; //O(1)

    }

    public int[] resultadosDiputados(int idDistrito)
    {

        int cantidadDeBancas = diputadosDeDistritos[idDistrito]; //O(1)

        if(resultadosBancasCalculado[idDistrito] == false) //O(1)
        {
            resultadosBancasCalculado[idDistrito] = true; //O(1)

            for(int i = 0; i < cantidadDeBancas; i++) //O(P-1)
            {
                int ordenDePartido = arrayDeHeaps[idDistrito].obtenerPartidoRaiz();  //O(1)
                bancasDiputados[idDistrito][ordenDePartido] += 1;  //O(1)
                int valorRecuperadoHeap = arrayDeHeaps[idDistrito].obtenerVotosRaiz() * bancasDiputados[idDistrito][ordenDePartido]; //O(1)
                int nuevoValorHeap = valorRecuperadoHeap / (bancasDiputados[idDistrito][ordenDePartido] + 1); //O(1)
                arrayDeHeaps[idDistrito].introducirNuevoValorRaiz(nuevoValorHeap); //O(Log (P-1))
            }
        }

        return bancasDiputados[idDistrito];    //O(1)                                                                                                     //O(1)

    }

    public boolean hayBallotage()
    {
        boolean ballotage = true;      //O(1)
        int porcentajeMayor = (primeroEnPresidenciales * 100) / totalVotosPresidenciales;     //O(1)
        int porcentajeDiferencia = porcentajeMayor - ((segundoEnPresidenciales * 100) / totalVotosPresidenciales);   //O(1)

        if(porcentajeMayor >= 45)  //O(1)
        {
            ballotage = false;   //O(1)
        }
        else if(porcentajeMayor > 40 && porcentajeDiferencia > 10)    //O(1)
        {
            ballotage = false;  //O(1)
        }

        return ballotage;   //O(1)

    }

    //Funciones Hechas por nosotros

    public int busquedaBinaria(int[] rangoMesas, int mesa)
    {
        int izquierda = 0;                                         //O(1)
        int derecha = rangoMesas.length - 1;                       //O(1)

        while(izquierda < derecha)                                 //O(Log(rangoMesas.lenght)) = O(Log(D))
        {
            int medio = izquierda + (derecha - izquierda) / 2;     //O(1)

            if(rangoMesas[medio] == mesa)                          //O(1)
            {
                return medio + 1;                                  //O(1)
            }
            else if(rangoMesas[medio] < mesa)                      //O(1)
            {
                izquierda = medio + 1;                             //O(1)
            }
            else                                                   //O(1)
            {
                derecha = medio;                                   //O(1)
            }
        }

        return izquierda;                                          //O(1)

    }
}

